// Cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Execption.h"
#include "square.h"
#include <exception>

int main()
{
	//square sq(-1, 3, 4);
	//square sq(2, -4, 2);
	square sq(-5, 6, -2);
	try {
		std::cout << sq.Zeros(1);
	}
	catch (MyException &myexc) {
		std::cout << myexc.what();
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
