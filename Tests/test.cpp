#include "pch.h"
#include "../Cpp/square.h"

TEST(TestCaseName, TwoZerosPlace) {
	square sq(-1, 3, 4);
	const double wynik = 4;
  EXPECT_EQ(sq.Zeros(1), wynik);
}

TEST(TestCaseName2, OneZerosPlace) {
	square sq(2, -4, 2);
	const int wynik = 1;
	EXPECT_EQ(sq.Zeros(1),wynik);
}

TEST(TestCaseName3, Test2) {
	square sq(-5, 6, -2);
	EXPECT_THROW(sq.Zeros(1) , MyException);
}
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}